import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.*;

import files.ReusableMethod;
import files.payload;
import static org.hamcrest.Matchers.*;

public class basics {

	public static void main(String[] args) {
		
		RestAssured.baseURI="https://rahulshettyacademy.com";
		String response= given().log().all().queryParam("key", "qaclick123").header("Content-Type","application/json")
		.body(payload.Addplace()).when().post("maps/api/place/add/json").then().log().all().assertThat().statusCode(200).extract().asString();
		
		//fetching the placeID
		System.out.println(response);
		JsonPath js= ReusableMethod.rawtoJson(response);
		String placeID= js.getString("place_id");
		System.out.println(placeID);
		
		//Update the Address and add above place id in the code
		String newAddress = "St Jones Road";
		given().log().all().queryParam("key", "qaclick123").header("Content-Type","application/json")
		.body("{\n" + 
				"\"place_id\":\""+placeID+"\",\n" + 
				"\"address\":\""+newAddress+"\",\n" + 
				"\"key\":\"qaclick123\"\n" + 
				"\n" + 
				"}\n" + 
				"").
		when().put("maps/api/place/update/json").
		then().log().all().assertThat().statusCode(200).body("msg",equalTo("Address successfully updated"));
		
		
		//Get and check the updated address
		String address1=   given().log().all().queryParam("key", "qaclick123").
				queryParam("place_id",placeID)
		.when().get("maps/api/place/get/json").then().log().all().assertThat().statusCode(200).extract()
		.response().asString();
		
		JsonPath js4= new JsonPath(address1);
		String updatedaddress = js4.getString("address");
		System.out.println(updatedaddress);

	}

}
